package com.mvc.twok.Controller;

import com.mvc.twok.Model.AccessLog;
import com.mvc.twok.Service.Interface.ILogProcessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/v1")
public class AccessLogController {
    private ILogProcessService accessLogService;

    @Autowired
    public void setAccessLogService(ILogProcessService accessLogService) {
        this.accessLogService = accessLogService;
    }

    @GetMapping("/accesslog")
    public String getEmployees()
    {
        return "Hello world";
    }

    @PostMapping("/accesslog")
    public ResponseEntity<List<AccessLog>> process() {
        List<AccessLog> list = accessLogService.processFileByFilePath("");
        if(list.size() == 0){
            return ResponseEntity.notFound().build();
        }else {
            return ResponseEntity.ok(list);
        }
    }

    @CrossOrigin(origins = "*")
    @PostMapping("/accesslog/upload")
    public ResponseEntity<List<AccessLog>> processF(@RequestParam("file") MultipartFile uploadfile) {
        if (uploadfile.isEmpty()) {
            return new ResponseEntity("please select a file!", HttpStatus.OK);
        }
        List<AccessLog> list;
        try {
            list = accessLogService.processFileByByteArray(uploadfile.getBytes());

        } catch (IOException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.ok(list);
    }

}
