package com.mvc.twok;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TwokApplication {
    public static void main(String[] args) {
        SpringApplication.run(TwokApplication.class, args);
    }
}
