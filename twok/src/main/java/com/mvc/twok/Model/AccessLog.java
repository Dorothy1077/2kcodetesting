package com.mvc.twok.Model;

public class AccessLog {
    private String clientIp;
    private String statusCode;
    private String method;
    private String sourceUrl;
    private String browser;

    public AccessLog(String clientIp, String statusCode, String method, String sourceUrl, String browser){
        this.clientIp = clientIp;
        this.statusCode = statusCode;
        this.method = method;
        this.sourceUrl = sourceUrl;
        this.browser = browser;
    }

    public String getClientIp() {
        return clientIp;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public String getMethod() {
        return method;
    }

    public String getSourceUrl() {
        return sourceUrl;
    }

    public String getBrowser() {
        return browser;
    }
}
