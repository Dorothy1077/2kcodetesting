package com.mvc.twok.Service;

import com.mvc.twok.Model.AccessLog;
import com.mvc.twok.Service.Interface.ILogProcessService;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class LogProcessServiceImpl implements ILogProcessService {
    @Override
    public List<AccessLog> processFileByFilePath(String filePath) {
        List<AccessLog> res = new ArrayList<>();
        BufferedReader reader;
        try {
            reader = new BufferedReader(new FileReader(
                    "/Users/bodong/Projects/2kcodetesting/access_log_20190520-125058.log"));
            res = processFile(reader);
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public List<AccessLog> processFileByByteArray(byte[] content) {
        List<AccessLog> res = new ArrayList<>();
        InputStream is = null;
        BufferedReader bfReader = null;
        try {
            is = new ByteArrayInputStream(content);
            bfReader = new BufferedReader(new InputStreamReader(is));
            res = processFile(bfReader);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try{
                if(is != null) is.close();
            } catch (Exception ex){

            }
        }
        return res;
    }

    private List<AccessLog> processFile(BufferedReader reader) throws IOException {
        List<AccessLog> res = new ArrayList<>();
        String line = reader.readLine();
        while (line != null) {
            res.add(parseStringToAccessLog(line));
            line = reader.readLine();
        }
        return res;
    }

    private AccessLog parseStringToAccessLog(String s){
        final String regex = "^([\\d.]+) (\\S+) (\\S+) \\[([\\w:/]+\\s[+\\-]\\d{4})\\] \"(.+?)\" (\\d{3}) (\\d+) \"([^\"]+)\" \"([^\"]+)\"";

        final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
        final Matcher matcher = pattern.matcher(s);

        AccessLog log = null;
        try{
            while (matcher.find()) {

                String iP = matcher.group(1);
                String method = matcher.group(5).split(" ")[0];
                String statusCode = matcher.group(6);
                String sourceUrl = matcher.group(8);
                String browser = matcher.group(9);
                log = new AccessLog(iP, statusCode, method, sourceUrl, browser);
            }
        }
        catch (Exception e){
            System.out.println(e);
        }
        return log;
    }
}
