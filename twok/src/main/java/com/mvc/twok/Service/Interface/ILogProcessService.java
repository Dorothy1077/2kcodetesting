package com.mvc.twok.Service.Interface;

import com.mvc.twok.Model.AccessLog;

import java.util.List;

public interface ILogProcessService {
    List<AccessLog> processFileByFilePath(String filePath);
    List<AccessLog> processFileByByteArray(byte[] content);
}
