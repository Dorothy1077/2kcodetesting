import React, { Component } from "react";
import "./App.css";
import Upload from "./upload/Upload";
import Report from "./report/Report";
import Table from "./Table"
class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="Card">
          <Upload />
        </div>
        <div className="Card">
          <Report />
          
        </div>
      </div>
    );
  }
}

export default App;
